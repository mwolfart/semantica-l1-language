open List

type variable = string

(* Outros operadores binário e unários podem ser adicionados a linguagem *)

type operator = Sum | Diff | Mult | Less | Leq | Eq | Neq | Geq | Greater | And | Or

type tipo  = TyInt | TyBool | TyFn of tipo * tipo | TyPair of tipo * tipo

type expr = Num of int
          | Bool of bool
          | Bop of operator * expr * expr
          | If of expr * expr * expr
          | Var of variable
          | App of expr * expr
          | Lam of variable * tipo * expr
          | Let of variable * tipo * expr * expr
          | Lrec of variable * tipo * tipo * variable * tipo * expr * expr
(*Pares Ordenados*)
          | Pair of expr * expr
          | Fst of expr
          | Snd of expr

type inst = INT of int 
          | BOOL of bool
          | POP
          | COPY
          | ADD
          | MULT
          | INV
          | EQ
          | GT
          | AND
          | NOT
          | JUMP of int
          | JMPIFTRUE of int
          | VAR of string
          | FUN of string * (inst list)                (* c = (Inst list) *)
          | RFUN of string * string * (inst list)
          | APPLY

type storableValue = Vnum of int
                   | Vbool of bool
                   | Vclos of e * string * (inst list)
                   | Vrclos of e * string * string * (inst list)
                   and  e = (string * storableValue) list

type stateValue = (c * s * e * (( c * s * e) list) ) 
          and c = inst list
          and s = storableValue list
          (* and e = (string * storableValue) list *)

exception NoRuleApplies of string

(* Aux functions *)
let rec drop n l = match l with
[] -> []
| h::t -> if n > 0 then drop (n-1) t else h::t

let rec lookup x ctx = (snd (find (fun (var,_) -> String.compare var x == 0) ctx))

let rec repeat exp n = if n = 0 then [] else exp @ repeat exp (n - 1) 

let rec compile t = match t with
                Num(n)  -> [INT n]
              | Bool(b) -> [BOOL b]
              | Bop(op,e1,e2) -> (
              let e1' = compile e1 in
                let e2' = compile e2 in (
                  match op with
                    Sum     -> e2' @ e1' @ [ADD] 
                  | Diff    -> e2' @ [INV] @ e1' @ [ADD]
                  | Mult    -> e2' @ e1' @ [MULT]
                  | Less    -> e1' @ e2' @ [GT]
                  | Leq     -> e2' @ e1' @ [GT] @ [NOT]
                  | Eq      -> e2' @ e1' @ [EQ]
                  | Neq     -> e2' @ e1' @ [EQ] @ [NOT]
                  | Geq     -> e1' @ e2' @ [GT] @ [NOT]
                  | Greater -> e2' @ e1' @ [GT]
                  | And     -> e2' @ e1' @ [AND]
                  | Or      -> e2' @ [NOT] @ e1' @ [NOT] @ [AND] @ [NOT]
                  )
              )
              | If(e1,e2,e3) -> (
                let e1' = compile e1 in 
                  let e2' = compile e2 in 
                    let e3' = compile e3 in (
                      e1' @ [JMPIFTRUE ((length e3') + 1)] @ e3' @ [JUMP (length e2')] @ e2'
                    )
              )
              | Var(x) -> [VAR x]
              | App(e1,e2) -> (
                let e1' = compile e1 in 
                  let e2' = compile e2 in  (
                    e2' @ e1' @ [APPLY]
                  )
              )
              | Lam(x,t,e) -> (
                let e' = compile e in 
                  [FUN(x,e')]
              )
                                
              | Let(x,t,e1,e2) -> (
                let e1'= compile e1 in
                  let e2' = compile e2 in (
                    e1' @ [FUN(x, e2')] @ [APPLY]
                  )
              )
              | Lrec(f,t1,t2,x,t1',e1,e2) -> (
                let e1' = compile e1 in 
                  let e2' = compile e2 in (
                    [RFUN(f,x,e1') ; FUN(f,e2'); APPLY]
                  )
              )
(*Pares Ordenados*)
              | Pair(e1,e2) -> (
                let e1' = compile e1 in
                  let e2' = compile e2 in (
                    [FUN("__pair__", 
                        [VAR "__pair__"] @ [JMPIFTRUE ((length e2') + 1)] @ e2' @ [JUMP (length e1')] @ e1')]
                  )
              )
              | Fst(e) -> (
                let e' = compile e in (
                  [BOOL true] @ e' @ [APPLY]
                )
              )
              | Snd(e) -> (
                let e' = compile e in (
                  [BOOL false] @ e' @ [APPLY]
                )
              ) 

let compile_step state = match state with
                        ( INT z::c, s, e, d ) -> (c, Vnum z::s, e, d )
                      | ( BOOL b::c, s, e, d ) -> (c, Vbool b::s, e, d )
                      | ( POP::c, sv::s, e, d ) -> (c, s, e, d)
                      | ( COPY::c, sv::s, e, d ) -> (c, sv::sv::s, e, d)
                      | ( ADD::c, Vnum z1::Vnum z2::s, e, d ) -> (c, Vnum(z1+z2)::s, e, d)
                      | ( MULT::c, Vnum z1::Vnum z2::s, e, d ) -> (c, Vnum(z1*z2)::s, e, d)
                      | ( INV::c, Vnum z::s, e, d ) -> (c, Vnum(-z)::s, e, d)
                      | ( EQ::c, Vnum z1::Vnum z2::s, e, d ) -> let eq = z1 == z2 in (c, Vbool eq :: s, e, d)
                      | ( GT::c, Vnum z1::Vnum z2::s, e, d ) -> let eq = z1 > z2 in (c, Vbool eq :: s, e, d)
                      | ( AND::c, Vbool b1::Vbool b2::s, e, d ) -> let b = b1 && b2 in (c, Vbool b::s, e, d)
                      | ( NOT::c, Vbool b::s, e, d ) -> (c, Vbool(not b)::s, e, d )
                      | ( JUMP n::c, s, e, d ) -> ( let valid = n <= (length c) in
                                                        if valid then 
                                                        ((drop n c), s, e, d) else
                                                        raise (NoRuleApplies "Jumping to a out of instructions position"))
                      | ( JMPIFTRUE n::c, Vbool b1::s, e, d ) -> ( let valid = n <= (length c) in 
                                                                          if valid && b1 then
                                                                          ((drop n c), s, e, d) 
                                                                          else if valid then
                                                                          (c, s, e, d)
                                                                          else raise (NoRuleApplies "Jumping to a out of instructions position"))
                      | ( VAR x::c, s, e, d ) -> (c, (lookup x e)::s, e, d )

                      (* These are for stacking regular functions *)
                      | ( FUN(x,c')::c, s, e, d ) -> (c, Vclos(e, x, c')::s, e, d)
                      | ( APPLY::c, Vclos(e', x, c')::sv::s, e, d ) -> (c', [], (x,sv)::e', (c,s,e)::d)

                      (* These are for stacking recursive functions *)
                      | ( RFUN(f,x, c')::c, s, e, d ) -> (c, Vrclos(e, f, x, c')::s, e, d)
                      | ( APPLY::c, Vrclos(e', f, x, c')::sv::s, e, d ) -> (c', [], (f,Vrclos(e', f, x, c'))::(x,sv)::e', (c, s, e)::d)

                      | ([], sv::[], e, (c', s', e')::d ) -> (c', sv::s', e', d)                         (*Ponto de retorno para funcoes*)

                      | _ -> raise (NoRuleApplies "Invalid instruction !")


let rec _run state = match state with
                    ([], stack , env, []) -> (hd stack)
                    | _ -> _run (compile_step state) 


let run instructions = _run (instructions, [], [], []) 
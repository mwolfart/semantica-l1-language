(* Gramatica : t ::= true | false | if ( t1 , t2 , t3 ) | 0 | succ ( t ) | pred ( t ) | iszero ( t ) *)
type term =
  TmTrue
  | TmFalse
  | TmIf of term * term * term
  | TmZero
  | TmSucc of term
  | TmPred of term
  | TmIsZero of term

(* Excecao a ser ativada quando termo for uma FORMA NORMAL *)
exception NoRuleApplies

(* Funcao auxiliar para determinar se um termo e um VALOR NUMERICO *)
let rec isnumericval t = match t with
  TmZero -> true
  | TmSucc ( t1 ) -> isnumericval t1
  | _ -> false


(* Implementacao da funcao STEP de avaliacao em um passo *)
let rec step t = match t with

(* CASO IF ( t1 , t2 , t3 ) *)
  TmIf ( TmTrue , t2 , t3 ) -> t2                   (* regra E−IfTrue *)
  | TmIf ( TmFalse , t2 , t3 ) -> t3                (* regra E−False *)
  | TmIf ( t1 , t2 , t3 ) -> let t1' =  step t1 in  (* regra E−If *)
                                        TmIf ( t1', t2 , t3 )

(* CASO SUCC ( t1 ) *)
  | TmSucc ( t1 ) -> let t1' = step t1 in(* regra E−Succ *)
                               TmSucc ( t1')

(* CASO PRED ( t1 ) *)
  | TmPred ( TmZero ) -> TmZero    (* regra E−PredZero *)
  | TmPred ( TmSucc ( nv1 )) when ( isnumericval nv1 ) -> nv1 (* regra E−PredSucc *)
  | TmPred ( t1 ) ->   let t1' = step t1 in       (* regra E−Pred *)
                                 TmPred ( t1')


  (* CASO ISZERO ( t1 ) *)
  | TmIsZero ( TmZero ) -> TmTrue (* regra E−IsZeroZero *)
  | TmIsZero ( TmSucc ( nv1 )) when ( isnumericval nv1 ) -> TmFalse (* regra E−IsZeroSucc *)
  | TmIsZero ( t1 ) -> let t1' = step t1 in(* regra E−IsZero *)
                                 TmIsZero ( t1')

  (* CASO Nenhuma regra se aplique ao termo *)
  | _ -> raise NoRuleApplies


  (* Implementacao de EVAL *)
  let rec eval t =
    try let t' = step t
    in eval t'
    with NoRuleApplies -> t

type variable = string

(* Outros operadores binário e unários podem ser adicionados a linguagem *)

type operator = Sum | Diff | Mult | Less | Leq | Eq | Neq | Geq | Greater | And | Or

type tipo  = TyInt | TyBool | TyFn of tipo * tipo | TyPair of tipo * tipo

type expr = Num of int
          | Bool of bool
          | Bop of operator * expr * expr
          | If of expr * expr * expr
          | Var of variable
          | App of expr * expr
          | Lam of variable * tipo * expr
          | Let of variable * tipo * expr * expr
          | Lrec of variable * tipo * tipo * variable * tipo * expr * expr
(*Pares Ordenados*)
          | Pair of expr * expr
          | Fst of expr
          | Snd of expr

type value = Vnum of int
           | Vbool of bool
           | Vclos of variable * expr * env
           | Vrclos of variable * variable * expr * env
           | Vpair of value * value
and
     env = (variable * value) list
and
     t_env = (variable * tipo) list     

exception NoRuleApplies of string

(* Avaliador big step para l1 *)
let rec _eval ctx t = match t with
        Num(n)  -> Vnum(n)
      | Bool(b) -> Vbool(b)
      | Bop(op,e1,e2) -> (
        let e1' = _eval ctx e1 in
          let e2' = _eval ctx e2 in (
            match (op, e1', e2') with
              (Sum, Vnum nr1, Vnum nr2)     -> Vnum(nr1 + nr2)
            | (Diff, Vnum nr1, Vnum nr2)    -> Vnum(nr1 - nr2)
            | (Mult, Vnum nr1, Vnum nr2)    -> Vnum(nr1 * nr2)
            | (Less, Vnum nr1, Vnum nr2)    -> Vbool(nr1 < nr2)
            | (Leq, Vnum nr1, Vnum nr2)     -> Vbool(nr1 <= nr2)
            | (Eq, Vnum nr1, Vnum nr2)      -> Vbool(nr1 == nr2)
            | (Neq, Vnum nr1, Vnum nr2)     -> Vbool(nr1 != nr2)
            | (Geq, Vnum nr1, Vnum nr2)     -> Vbool(nr1 >= nr2)
            | (Greater, Vnum nr1, Vnum nr2) -> Vbool(nr1 > nr2)
            | (And, Vbool b1, Vbool b2)     -> Vbool(b1 && b2)
            | (Or, Vbool b1, Vbool b2)      -> Vbool(b1 || b2)
            | _ -> raise (NoRuleApplies "Invalid binary operation.")
            )
        )
      | If(e1,e2,e3) -> (
        let e1' = _eval ctx e1 in (
            match e1' with
              (Vbool true)  -> _eval ctx e2
            | (Vbool false) -> _eval ctx e3
            | _ -> raise(NoRuleApplies "Invalid condition to if/else.")
          )
        )

      | Var(x) -> (snd (List.find (fun (var,_) -> String.compare var x == 0) ctx)) (* percorre a lista e ve quando
                                                                                    da match em val, retorna var*)

      | App(e1,e2) -> (
          let e1' = _eval ctx e1 in
            let e2' = _eval ctx e2 in (
                match(e1', e2') with
                | (Vclos(x, e, amb), v) -> let ctx' =  (x,v)::amb in _eval ctx' e
                | (Vrclos(f, x, e, amb), v) -> let ctx' = (f,Vrclos(f, x, e, amb))::(x,v)::amb in _eval ctx' e
                | _ -> raise(NoRuleApplies "Invalid Application")
              )
        )
      | Lam(x,t,e) -> Vclos(x, e, ctx)
      | Let(x,t,e1,e2) -> (
          let ctx' = (x,(_eval ctx e1))::ctx in _eval ctx' e2
        )
      | Lrec(f,t1,t2,x,t1',e1,e2) -> (
          let rclos = Vrclos(f, x, e1, ctx) in _eval ((f,rclos)::ctx) e2
        )
      | Pair(e1, e2) -> (
        let e1' = _eval ctx e1 in
          let e2' = _eval ctx e2 in (
            Vpair(e1', e2')
          )
      )

      | Fst(e) -> (
        let e' = _eval ctx e in (
          match e' with 
          | (Vpair(e1, _)) -> e1
          | _ -> raise(NoRuleApplies "Invalid use of pair (Fst (not a pair))")
        )
      )

      | Snd(e) -> (
        let e' = _eval ctx e in (
          match e' with 
          | (Vpair(_, e2)) -> e2
          | _ -> raise(NoRuleApplies "Invalid use of pair (Fst (not a pair))")
        )
      )


let eval t = _eval [] t 



(* typeinfer *)

let rec typeinfer (ctx : t_env) (exp : expr) : tipo =
    match exp with
          Num(n)      -> TyInt
        | Bool(n)   -> TyBool
            
        | Bop(op, e1, e2) ->
            let e1' = typeinfer ctx e1 in
            let e2' = typeinfer ctx e2 in
            (match (op, e1', e2') with
                  (Sum, TyInt, TyInt)           -> TyInt
                | (Diff, TyInt, TyInt)        -> TyInt
                | (Mult, TyInt, TyInt)        -> TyInt
                | (Less, TyInt, TyInt)        -> TyBool
                | (Leq, TyInt, TyInt)         -> TyBool
                | (Eq, TyInt, TyInt)          -> TyBool
                | (Neq, TyInt, TyInt)         -> TyBool
                | (Geq, TyInt, TyInt)         -> TyBool
                | (Greater, TyInt, TyInt)     -> TyBool
                | (Eq, TyBool, TyBool)        -> TyBool
                | (Neq, TyBool, TyBool)       -> TyBool
                | (And, TyBool, TyBool)       -> TyBool
                | (Or, TyBool, TyBool)        -> TyBool
                | _ -> raise(NoRuleApplies "Invalid binary operation.")
            )
            
        | If(cond, eT, eF) ->
            if (typeinfer ctx cond) = TyBool
            then
            	let eT' = (typeinfer ctx eT) in
                if eT' = (typeinfer ctx eF)
                    then eT'
                else raise(NoRuleApplies "If clauses have different types.")
            else raise(NoRuleApplies "If conditional is not boolean.")
                            
        | Var(x) -> (snd (List.find (fun (var,_) -> String.compare var x == 0) ctx))
       
        | App(e1, e2) ->
            let e1' = typeinfer ctx e1 in
            let e2' = typeinfer ctx e2 in
            (match e1' with
                TyFn(t1, t2) -> (if t1 = e2'
                                then t2
                                else raise(NoRuleApplies "Application: function input doesn't match."))
                | _ -> raise(NoRuleApplies "Application: left expression is not a function.")
            )
                      
        | Lam(var, tVar, e) -> TyFn(tVar, (typeinfer ((var, tVar)::ctx) e))
        
        | Let(var, tVar, expVar, e) ->
            let expVar' = typeinfer ctx expVar in
            let e' = typeinfer ((var, tVar)::ctx) e in
            (if expVar' = tVar then e' else raise(NoRuleApplies "Invalid Let."))
            
        | Lrec(func, tFuncIn, tFuncOut, var, tVar, expVar, e) ->
            (if tVar = tFuncIn then
                let expVar' = typeinfer ((var, tFuncIn)::(func, TyFn(tFuncIn, tFuncOut))::ctx) expVar in
                let e' = typeinfer ((func, TyFn(tFuncIn, tFuncOut))::ctx) e in
                (if expVar' = tFuncOut then e' else raise(NoRuleApplies "Invalid LetRec."))
            else raise(NoRuleApplies "LetRec: function input doesn't match.")
            )
(*Pares Ordenados*) 
        | Pair(e1, e2) -> (
            let e1' = typeinfer ctx e1 in
                let e2' = typeinfer ctx e2 in (
                (if e1' = e2' then TyPair(e1', e2') else raise(NoRuleApplies "Pairs types must be the same !"))
                )
            )
    
        | Fst(e) -> (
            let e' = typeinfer ctx e in (
                match e' with 
                | (TyPair(e1, _)) -> e1
                | _ -> raise(NoRuleApplies "Wrong use of pair")
                )
            )

        | Snd(e) -> (
            let e' = typeinfer ctx e in (
                match e' with 
                | (TyPair(_, e2)) -> e2
                | _ -> raise(NoRuleApplies "Wrong use of pair")
                )
            )
        

let ti (e : expr) : tipo = typeinfer [] e
open OUnit
open Typeinfer

(************************ EXPRESSIONS TO BE TESTED ***************************)
(* Constructors *)
let expNumConstructor = (Num (-8))
let expBoolConstructor = (Bool (true))

(* Bops *)
let expBopSum  = (Bop(Sum, Num(-3), Num(5)))                                        (* -3 + 5 *)
let expBopDiff = (Bop(Diff, Num(-3), Num(-20)))                                     (* -3 - -20 *)
let expBopMult = (Bop(Mult, Num(99999999999), Num(-31982982)))                      (* 99999999999 * -31982982 *)
let expBopLess = (Bop(Less, Num(3), Num(-5)))                                       (* 3 < -5*)
let expBopLeq = (Bop(Leq, Num(-3), Num(-3)))                                        (* -3 <= -3*)
let expBopEq = (Bop(Eq, Num(5), Num(-17)))                                          (* 5 == -17*)
let expBopNeq = (Bop(Neq, Num(5), Num(5)))                                          (* 5 != 5*)
let expBopGeq = (Bop(Geq, Num(-3), Num(-10)))                                       (* -3 => -5 *)
let expBopGreater = (Bop(Greater, Num(42), Num(42)))                                (* 42 > 42 *)
let expBopNested = (Bop(Greater,                                                    (*(3 * (100-2) - (42+42)) > (3*17 - 5) *)
                      Bop(Diff,
                        Bop(Mult,
                          Num(3),
                          Bop(Diff,
                            Num(100),
                            Num(2)
                          )
                        ),
                        Bop(Sum, Num(42), Num(42))
                      ),
                      Bop(Diff,
                        Bop(Mult, Num(3), Num(17)),
                        Num(5))))

(* Ifs *)
let expSimpleIfInt = (If((Bool true),
                        Num(0),
                        Num(2)))

let expSimpleIfBool = (If((Bool false),
						Bool(true),
						Bool(false)))

let expSimpleIfBadType = (If((Bool false),
                            Bool(true),
                            Num(3)))       

let expSimpleIfInvalid = (If((Num(3)),
                            Num(5),
                            Num(3)))                   


(* Lambdas *)

let expFuncSucc = (Lam("x", TyInt, Bop(Sum, Var("x"), Num(1))))

let expFuncPred = (Lam("x", TyInt, Bop(Diff, Var("x"), Num(1))))

let expFuncIsTrue = (Lam("x", TyBool, Bop(Eq, Var("x"), Bool(true))))

(* Application *)

let expAppValid = (App(expFuncSucc, Num(5)))

let expAppInvalidFunction = (App(Num(2), Num(5)))

let expAppInvalidInput = (App(expFuncIsTrue, Num(3)))

(* Lets *)

let expLetValidInt = (Let("x", TyInt, Num(5), Var("x")))

let expLetValidBoolValidIf = (Let("x", TyBool, Bool(false), If(Var("x"), Num(5), Num(4))))

let expLetValidBoolBadIf = (Let("x", TyBool, Bool(false), If(Var("x"), Num(5), Bool(false))))

let expLetInvalidVarType = (Let("x", TyInt, Num(5), If(Var("x"), Num(5), Num(4))))

let expLetInvalidNonExistentVar = (Let("x", TyInt, Num(5), Var("y")))

let expLetInvalidDeclaration = (Let("x", TyInt, Bool(true), Var("x")))

(* LetRecs *)

(* TODO *)

(* Composed functions *)

let expFat = (Lrec("fat", TyInt, TyInt, "x", TyInt,
                If(Bop(Eq, Var("x"), Num(0)),
                  Num(1),
                  Bop(Mult, Var("x"), App(Var("fat"), Bop(Diff, Var("x"), Num(1))))),
                App(Var("fat"), Num(6))))

let expFib = (Lrec("fib", TyInt, TyInt, "x", TyInt,
                If(Bop(Leq, Var("x"),Num(1)),
                    Num(1),
                    Bop(Sum, App(Var("fib"), Bop(Diff, Var("x"), Num(1))) , App(Var("fib"), Bop(Diff, Var("x"), Num(2))))),
                App(Var("fib"), Num(7))))

let expIsEven1 = (Lrec("iseven", TyInt, TyBool, "a", TyInt,
                    If(Bop(Eq, Var("a"), Num(1)),
                      Bool(false),
                          If(Bop(Eq, Var("a"), Num(0)),
                          Bool(true),
                          App(Var("iseven"),Bop(Diff, Var("a"), Num(2))))),
                    App(Var("iseven"), Num(3))))

let expIsEven2 = (Lrec("iseven", TyInt, TyBool, "a", TyInt,
                    If(Bop(Eq, Var("a"), Num(1)),
                      Bool(false),
                          If(Bop(Eq, Var("a"), Num(0)),
                          Bool(true),
                          App(Var("iseven"),Bop(Diff, Var("a"), Num(2))))),
                    App(Var("iseven"), Num(300))))


let expSoma = (Lrec("soma", TyInt, TyFn(TyInt, TyInt), "a", TyInt,
                Lam("b", TyInt,
                  If(Bop(Eq, Var("b"), Num(0)),
                    Var("a"),
                    App(App(Var("soma"), Bop(Sum, Var("a"), Num(1))), Bop(Diff, Var("b"), Num(1))))
                ),
                App(App(Var("soma"), Num(-3)), Num(17))))
    

let expVezes = (Lrec("vezes", TyInt, TyFn(TyInt, TyInt), "a", TyInt,
                  Lam("b", TyInt,
                      If( Bop(Eq, Var("a"), Num(0)),
                          Num(0),
                          If( Bop(Eq, Var("b"), Num(0)),
                              Num(0),
                              Bop(Sum, Var("b"), App(App(Var("vezes"), Bop(Diff, Var("a"), Num(1))), Var("b")))
                          )
                      )
                  ),
                  App(App(Var("vezes"), Num(3)), Num(6))))

let pairSimpleFst = (Fst (Pair( Bop(Sum, Num 1, Num 2), Num 2)))
let pairSimpleSnd = (Snd (Pair( Bop(Sum, Num 1, Num 2), Num 2))) (*Embora não leve a erro de execução, não é bem tipado !*)
let pairDif = (Fst(Pair(Num 2, Bool true)))

let pairWithOpt = (Let("pair", TyPair(TyInt, TyInt ), (Pair(Num 2, Num 4)), Bop(Sum, Fst(Var("pair")), Snd(Var("pair")))))

let twoPairs = (Let("pair1", TyPair(TyInt, TyInt), (Pair(Num 2, Num 4)),
                  (Let("pair2", TyPair(TyInt, TyInt), (Pair(Num 3, Num 4)),  
                    Bop(Sum, Bop(Mult, Fst(Var("pair1")), Fst(Var("pair2"))), Bop(Mult, Snd(Var("pair2")), Snd(Var("pair2")))
                    )
                  )
                  )
                )
              ) 


(************************        TYPEINFER TESTS          ********************)
(************************ TEST FOR BASIC CONSTRUCTORS ************************)

let testNumConstructor test_ctxt = assert_equal (TyInt) (ti expNumConstructor) (*constructs integer -8*)
let testBoolConstructor test_ctxt = assert_equal (TyBool) (ti expBoolConstructor) (*constructs bool true*)

(************************ TEST FOR BINARY OPERATIONS ************************)

let testBopSum' = ti expBopSum                                                      
let testBopSum test_ctxt = assert_equal (TyInt) testBopSum'                       (* TyInt *)

let testBopDiff' = ti expBopDiff                                                    
let testBopDiff test_ctxt = assert_equal (TyInt) testBopDiff'                     (* TyInt *)

let testBopMult' = ti expBopMult                                                    
let testBopMult test_ctxt = assert_equal (TyInt) testBopMult'  				      (* TyInt *)

let testBopLess' = ti expBopLess                                                    
let testBopLess test_ctxt = assert_equal (TyBool) testBopLess'                   (* TyBool *)

let testBopLeq' = ti expBopLeq                                                      
let testBopLeq test_ctxt = assert_equal  (TyBool) testBopLeq'                    (* TyBool *)

let testBopEq' = ti expBopEq                                                        
let testBopEq test_ctxt = assert_equal (TyBool) testBopEq'                       (* TyBool *)

let testBopNeq' = ti expBopNeq                                                      
let testBopNeq test_ctxt = assert_equal (TyBool) testBopNeq'                     (* TyBool *)

let testBopGeq' = ti expBopGeq                                                      
let testBopGeq test_ctxt = assert_equal (TyBool) testBopGeq'                     (* TyBool *)

let testBopGreater' = ti expBopGreater                                              
let testBopGreater test_ctxt = assert_equal  (TyBool) testBopGreater'            (* TyBool *)

let testNestedBops' = ti expBopNested 
let testNestedBops test_ctxt = assert_equal (TyBool) testNestedBops'


let testFailBop1' = fun () -> ti (                                 (*Like in QUnit when testing for raises should use currying for function*)
                          Bop(Sum, Bool(true), Num(1))                  (*True + 1 should fail*)
                            )
let testFailBop1 test_ctxt = assert_raises (NoRuleApplies "Invalid binary operation.") testFailBop1'

(************************* TEST FOR IF/ELSEs *************************)
let testSimpleIfInt' = ti expSimpleIfInt
let testSimpleIfInt test_ctxt = assert_equal (TyInt) testSimpleIfInt'

let testSimpleIfBool' = ti expSimpleIfBool
let testSimpleIfBool test_ctxt = assert_equal (TyBool) testSimpleIfBool'

let testSimpleIfBadType' = fun () -> ti expSimpleIfBadType
let testSimpleIfBadType test_ctxt = assert_raises (NoRuleApplies "If clauses have different types.") testSimpleIfBadType'

let testSimpleIfInvalid' = fun () -> ti expSimpleIfInvalid
let testSimpleIfInvalid test_ctxt = assert_raises (NoRuleApplies "If conditional is not boolean.") testSimpleIfInvalid'

(************************* VAR ACCESS ***********************)

let testVarAccess' = (typeinfer [("x", TyInt)] (Var("x")))
let testVarAccess test_ctxt = assert_equal (TyInt) testVarAccess'

let testVarInvalid' = fun () -> (typeinfer [("x", TyInt)] (Var("y")))
let testVarInvalid test_ctxt = assert_raises (Not_found) testVarInvalid'

(************************** LAMBDA ******************************)

let testFuncSucc' = ti expFuncSucc
let testFuncSucc test_ctxt = assert_equal (TyFn(TyInt, TyInt)) testFuncSucc'

let testFuncPred' = ti expFuncPred
let testFuncPred test_ctxt = assert_equal (TyFn(TyInt, TyInt)) testFuncPred'

let testFuncIsTrue' = ti expFuncIsTrue
let testFuncIsTrue test_ctxt = assert_equal (TyFn(TyBool, TyBool)) testFuncIsTrue'

(*********************** APPLICATION ****************************)

let testAppValid' = ti expAppValid
let testAppValid test_ctxt = assert_equal (TyInt) testAppValid'

let testAppInvalidInput' = fun () -> ti expAppInvalidInput
let testAppInvalidInput test_ctxt = assert_raises (NoRuleApplies "Application: function input doesn't match.") testAppInvalidInput'

let testAppInvalidFunction' = fun () -> ti expAppInvalidFunction
let testAppInvalidFunction test_ctxt = assert_raises (NoRuleApplies "Application: left expression is not a function.") testAppInvalidFunction'

(************************** LET *********************************)

let testLetValidInt' = ti expLetValidInt
let testLetValidInt test_ctxt = assert_equal (TyInt) testLetValidInt'

let testLetValidBoolValidIf' = ti expLetValidBoolValidIf
let testLetValidBoolValidIf test_ctxt = assert_equal (TyInt) testLetValidBoolValidIf'

let testLetValidBoolBadIf' = fun () -> ti expLetValidBoolBadIf
let testLetValidBoolBadIf test_ctxt = assert_raises (NoRuleApplies "If clauses have different types.") testLetValidBoolBadIf'

let testLetInvalidVarType' = fun () -> ti expLetInvalidVarType
let testLetInvalidVarType test_ctxt = assert_raises (NoRuleApplies "If conditional is not boolean.") testLetInvalidVarType'

let testLetInvalidNonExistentVar' = fun () -> ti expLetInvalidNonExistentVar
let testLetInvalidNonExistentVar test_ctxt = assert_raises (Not_found) testLetInvalidNonExistentVar'

let testLetInvalidDeclaration' = fun () -> ti expLetInvalidDeclaration
let testLetInvalidDeclaration test_ctxt = assert_raises (NoRuleApplies "Invalid Let.") testLetInvalidDeclaration'

(********************* EXERCISE TEST FUNCTIONS ******************)

let testFat' = ti expFat
let testFat test_ctxt = assert_equal (TyInt) testFat'

let testFib' = ti expFib
let testFib test_ctxt = assert_equal (TyInt) testFib'

let testIsEven1' = ti expIsEven1
let testIsEven1 test_ctxt = assert_equal (TyBool) testIsEven1'

let testIsEven2' = ti expIsEven2
let testIsEven2 test_ctxt = assert_equal (TyBool) testIsEven2'

let testSoma' = ti expSoma
let testSoma test_ctxt = assert_equal (TyInt) testSoma'

let testVezes' = ti expVezes
let testVezes test_ctxt = assert_equal (TyInt) testVezes' 

let testPairFst' = ti pairSimpleFst
let testPairFst test_ctxt = assert_equal (TyInt) testPairFst'

let testPairSnd' = ti pairSimpleSnd
let testPairSnd test_ctxt = assert_equal (TyInt) testPairSnd'

let testPairWithOpt' = ti pairWithOpt
let testPairWithOpt test_ctxt = assert_equal (TyInt) testPairWithOpt'

let testTwoPairs' = ti twoPairs
let testTwoPairs test_ctxt = assert_equal (TyInt) testTwoPairs'

let testPairDif' = fun () -> ti pairDif
let testPairDif test_ctxt = assert_raises (NoRuleApplies "Pairs types must be the same !") testPairDif'


(************************      TYPEINFER TESTS       ***************************)
(************************ TEST FOR BASIC CONSTRUCTORS ************************)


let suite =
"suite" >:::
 [
  "[TypeInfer] Should construct a TyInt" >:: testNumConstructor;
  "[TypeInfer] Should construct a TyBool" >:: testBoolConstructor;
  "[TypeInfer] Sum should return TyInt" >:: testBopSum;
  "[TypeInfer] Diff should return TyInt" >:: testBopDiff;
  "[TypeInfer] Mult should return TyInt" >:: testBopMult;
  "[TypeInfer] Less should return TyBool" >:: testBopLess;
  "[TypeInfer] Leq should return TyBool" >:: testBopLeq;
  "[TypeInfer] Eq should return TyBool" >:: testBopEq;
  "[TypeInfer] Neq should return TyBool" >:: testBopNeq;
  "[TypeInfer] Geq should return TyBool" >:: testBopGeq;
  "[TypeInfer] Greater should return TyBool" >:: testBopGreater;
  "[TypeInfer] Nested bops should return TyBool" >:: testNestedBops;
  "[TypeInfer] Should raise NoRuleApplies" >:: testFailBop1;
  "[TypeInfer] If should be integer" >:: testSimpleIfInt;
  "[TypeInfer] If should be boolean" >:: testSimpleIfBool;
  "[TypeInfer] If should raise bad typing" >:: testSimpleIfBadType;
  "[TypeInfer] If should be invalid" >:: testSimpleIfInvalid;
  "[TypeInfer] Variable should be of TyInt" >:: testVarAccess;
  "[TypeInfer] Variable should raise not found" >:: testVarInvalid;
  "[TypeInfer] FuncSucc should be TyInt -> TyInt" >:: testFuncSucc;
  "[TypeInfer] FuncPred should be TyInt -> TyInt" >:: testFuncPred;
  "[TypeInfer] FuncIsTrue should be TyBool -> TyBool" >:: testFuncIsTrue;
  "[TypeInfer] App should be valid and of TyInt" >:: testAppValid;
  "[TypeInfer] App should raise invalid input" >:: testAppInvalidInput;
  "[TypeInfer] App should raise invalid function" >:: testAppInvalidFunction;
  "[TypeInfer] Let should be of TyInt" >:: testLetValidInt;
  "[TypeInfer] Let should be of TyInt" >:: testLetValidBoolValidIf;
  "[TypeInfer] Let should raise invalid if" >:: testLetValidBoolBadIf;
  "[TypeInfer] Let should raise invalid if" >:: testLetInvalidVarType;
  "[TypeInfer] Let should raise invalid let" >:: testLetInvalidNonExistentVar;
  "[TypeInfer] Let should raise invalid let" >:: testLetInvalidDeclaration;
  "[TypeInfer] Should return 6! = TyInt" >:: testFat;
  "[TypeInfer] Should return fib(7) = TyInt" >:: testFib;
  "[TypeInfer] Should return iseven(3) = TyBool">::testIsEven1;
  "[TypeInfer] Should return iseven(300) = TyBool">::testIsEven2;
  "[TypeInfer] Should return -3 + 18 = TyInt">::testSoma;
  "[TypeInfer] Should return 3*6 = TyInt">::testVezes;
  "[TypeInfer] Should return the first element of pair" >:: testPairFst;
  "[TypeInfer] Should return the second element of pair" >:: testPairSnd;
  "[TypeInfer] Tests pair in middle of let expression" >:: testPairWithOpt;
  "[TypeInfer] Check if you can 'nest' pairs" >:: testTwoPairs;
  "[TypeInfer] Check if evaluates for pair with different types" >:: testPairDif;
 ]


let _ =
  run_test_tt_main suite
open OUnit
open Eval


(************************ EXPRESSIONS TO BE TESTED ***************************)
(* Constructors *)
let expNumConstructor = (Num (-8))
let expBoolConstructor = (Bool (true))

(* Bops *)
let expBopSum  = (Bop(Sum, Num(-3), Num(5)))                                        (* -3 + 5 *)
let expBopDiff = (Bop(Diff, Num(-3), Num(-20)))                                     (* -3 - -20 *)
let expBopMult = (Bop(Mult, Num(99999999999), Num(-31982982)))                      (* 99999999999 * -31982982 *)
let expBopLess = (Bop(Less, Num(3), Num(-5)))                                       (* 3 < -5*)
let expBopLeq = (Bop(Leq, Num(-3), Num(-3)))                                        (* -3 <= -3*)
let expBopEq = (Bop(Eq, Num(5), Num(-17)))                                          (* 5 == -17*)
let expBopNeq = (Bop(Neq, Num(5), Num(5)))                                          (* 5 != 5*)
let expBopGeq = (Bop(Geq, Num(-3), Num(-10)))                                       (* -3 => -5 *)
let expBopGreater = (Bop(Greater, Num(42), Num(42)))                                (* 42 > 42 *)
let expBopNested = (Bop(Greater,                                                    (*(3 * (100-2) - (42+42)) > (3*17 - 5) *)
                      Bop(Diff,
                        Bop(Mult,
                          Num(3),
                          Bop(Diff,
                            Num(100),
                            Num(2)
                          )
                        ),
                        Bop(Sum, Num(42), Num(42))
                      ),
                      Bop(Diff,
                        Bop(Mult, Num(3), Num(17)),
                        Num(5))))

(* Ifs *)
let expSimpleIfTrue = (If((Bool true),                                     (*if true *)
                        Num(0),                                            (*then 0 *)
                        Bop(Diff, Num(-3), Num(20))))                      (*else -3 -20*)

let expSimpleIfFalse = (If((Bool false),                                   (*if false *)
                            Num(0),                                        (*then 0 *)
                            Bop(Diff, Num(-3), Num(20))))                  (*else -3 -20*)
                        
let expNestedIf = (If( Bop(Geq, Num(8), Num(10)),                         (*If 8 > 10*)
                      If(Bop(Geq, Num(8), Num(7)),                        (*(then)should ignore this*)
                          Num(5),
                          Num(6)),
                      If(Bop(Eq, Num(3), Num(3)),                         (*else if 3 == 3*)
                            If(Bop(Neq, Num(5), Num(5)),                  (*  then if 5 != 5*)
                              Num(0),
                              Num(1)                                      (*should stop here, just think the logic throught*)
                            ),
                            Num(-2))))

(* Composed functions *)

let expFat = (Lrec("fat", TyInt, TyInt, "x", TyInt,
                If(Bop(Eq, Var("x"), Num(0)),
                  Num(1),
                  Bop(Mult, Var("x"), App(Var("fat"), Bop(Diff, Var("x"), Num(1))))),
                App(Var("fat"), Num(6))))


let expFib = (Lrec("fib", TyInt, TyInt, "x", TyInt,
                If(Bop(Leq, Var("x"),Num(1)),
                    Num(1),
                    Bop(Sum, App(Var("fib"), Bop(Diff, Var("x"), Num(1))) , App(Var("fib"), Bop(Diff, Var("x"), Num(2))))),
                App(Var("fib"), Num(7))))

let expIsEven1 = (Lrec("iseven", TyInt, TyBool, "a", TyInt,
                If(Bop(Eq, Var("a"), Num(1)),
                  Bool(false),
                      If(Bop(Eq, Var("a"), Num(0)),
                      Bool(true),
                      App(Var("iseven"),Bop(Diff, Var("a"), Num(2))))),
                App(Var("iseven"), Num(3))))

let expIsEven2 = (Lrec("iseven", TyInt, TyBool, "a", TyInt,
                If(Bop(Eq, Var("a"), Num(1)),
                  Bool(false),
                      If(Bop(Eq, Var("a"), Num(0)),
                      Bool(true),
                      App(Var("iseven"),Bop(Diff, Var("a"), Num(2))))),
                App(Var("iseven"), Num(300))))


let expSoma = (Lrec("soma", TyInt, TyFn(TyInt, TyInt), "a", TyInt,
                Lam("b", TyInt,
                  If(Bop(Eq, Var("b"), Num(0)),
                    Var("a"),
                    App(App(Var("soma"), Bop(Sum, Var("a"), Num(1))), Bop(Diff, Var("b"), Num(1))))
                ),
                App(App(Var("soma"), Num(-3)), Num(17))))
      
  
let expVezes = (Lrec("vezes", TyInt, TyFn(TyInt, TyInt), "a", TyInt,
                  Lam("b", TyInt,
                    If( Bop(Eq, Var("a"), Num(0)),
                      Num(0),
                      If( Bop(Eq, Var("b"), Num(0)),
                        Num(0),
                        Bop(Sum, Var("b"), App(App(Var("vezes"), Bop(Diff, Var("a"), Num(1))), Var("b")))
                      )
                    )
                ),
                App(App(Var("vezes"), Num(3)), Num(6))))

              
let pairSimpleFst = (Fst (Pair( Bop(Sum, Num 1, Num 2), Num 2)))
let pairSimpleSnd = (Snd (Pair( Bop(Sum, Num 1, Num 2), Num 2))) (*Embora não leve a erro de execução, não é bem tipado !*)
let pairDif = (Fst(Pair(Num 2, Bool true)))

let pairWithOpt = (Let("pair", TyPair(TyInt, TyInt ), (Pair(Num 2, Num 4)), Bop(Sum, Fst(Var("pair")), Snd(Var("pair")))))

let twoPairs = (Let("pair1", TyPair(TyInt, TyInt), (Pair(Num 2, Num 4)),
                  (Let("pair2", TyPair(TyInt, TyInt), (Pair(Num 3, Num 4)),  
                    Bop(Sum, Bop(Mult, Fst(Var("pair1")), Fst(Var("pair2"))), Bop(Mult, Snd(Var("pair2")), Snd(Var("pair2")))
                    )
                  )
                  )
                )
              ) 
                

(************************        EVAL TESTS          *************************)
(************************ TEST FOR BASIC CONSTRUCTORS ************************)

let testNumConstructor test_ctxt = assert_equal (Vnum (-8)) (eval expNumConstructor) (*constructs integer -8*)
let testBoolConstructor test_ctxt = assert_equal (Vbool true) (eval expBoolConstructor) (*constructs bool true*)

(************************ TEST FOR BINARY OPERATIONS ************************)

let testBopSum' = eval expBopSum                                                      
let testBopSum test_ctxt = assert_equal (Vnum (2)) testBopSum'                        (* = 2 *)

let testBopDiff' = eval expBopDiff                                                    
let testBopDiff test_ctxt = assert_equal (Vnum (17)) testBopDiff'                     (* = 17 *)

let testBopMult' = eval expBopMult                                                    
let testBopMult test_ctxt = assert_equal (Vnum (-3198298199968017018)) testBopMult'   (* =-3198298199968017018 *)

let testBopLess' = eval expBopLess                                                    
let testBopLess test_ctxt = assert_equal (Vbool false) testBopLess'                   (* false *)

let testBopLeq' = eval expBopLeq                                                      
let testBopLeq test_ctxt = assert_equal  (Vbool true) testBopLeq'                     (* true *)

let testBopEq' = eval expBopEq                                                        
let testBopEq test_ctxt = assert_equal (Vbool false) testBopEq'                       (* false *)

let testBopNeq' = eval expBopNeq                                                      
let testBopNeq test_ctxt = assert_equal (Vbool false) testBopNeq'                     (* false *)

let testBopGeq' = eval expBopGeq                                                      
let testBopGeq test_ctxt = assert_equal (Vbool true) testBopGeq'                      (* true *)

let testBopGreater' = eval expBopGreater                                              
let testBopGreater test_ctxt = assert_equal  (Vbool false) testBopGreater'            (* false *)

let testNestedBops' = eval expBopNested 
let testNestedBops test_ctxt = assert_equal (Vbool true) testNestedBops'


let testFailBop1' = fun () -> eval (                                 (*Like in QUnit when testing for raises should use currying for function*)
                          Bop(Sum, Bool(true), Num(1))                  (*True + 1 should fail*)
                            )
let testFailBop1 test_ctxt = assert_raises (NoRuleApplies "Invalid binary operation.") testFailBop1'


(************************* TEST FOR IF/ELSEs *************************)
let testSimpleIfTrue'  = eval expSimpleIfTrue
let testSimpleIfTrue'' = eval (Num (0))
let testSimpleIfTrue test_ctxt = assert_equal testSimpleIfTrue'' testSimpleIfTrue'

let testSimpleIfFalse'  = eval expSimpleIfFalse
let testSimpleIfFalse'' = Vnum(-23)
let testSimpleIfFalse test_ctxt = assert_equal testSimpleIfFalse'' testSimpleIfFalse'


let testNestedIfs'  = eval expNestedIf
let testNestedIfs test_ctxt = assert_equal (Vnum 1) testNestedIfs'



(* EXERCISE TEST FUNCTIONS *)

let testFat' = eval expFat
let testFat test_ctxt = assert_equal (Vnum 720) testFat'

let testFib' = eval expFib
let testFib test_ctxt = assert_equal (Vnum 21) testFib'

let testIsEven1' = eval expIsEven1
let testIsEven1 test_ctxt = assert_equal (Vbool false) testIsEven1'

let testIsEven2' = eval expIsEven2
let testIsEven2 test_ctxt = assert_equal (Vbool true) testIsEven2'

let testSoma' = eval expSoma
let testSoma test_ctxt = assert_equal (Vnum 14) testSoma'

let testVezes' = eval expVezes
let testVezes test_ctxt = assert_equal (Vnum 18) testVezes'

let testPairFst' = eval pairSimpleFst
let testPairFst test_ctxt = assert_equal (Vnum 3) testPairFst'

let testPairSnd' = eval pairSimpleSnd
let testPairSnd test_ctxt = assert_equal (Vnum 2) testPairSnd'

let testPairWithOpt' = eval pairWithOpt
let testPairWithOpt test_ctxt = assert_equal (Vnum 6) testPairWithOpt'

let testTwoPairs' = eval twoPairs
let testTwoPairs test_ctxt = assert_equal (Vnum 22) testTwoPairs'

let testPairDif' = eval pairDif
let testPairDif test_ctxt = assert_equal (Vnum 2) testPairDif'


(************************      COMPILE TESTS       ***************************)
(************************ TEST FOR BASIC CONSTRUCTORS ************************)


let suite =
"suite" >:::
 [
  "[Eval] Should construct a negative Vnum" >:: testNumConstructor;
  "[Eval] Should construct a true Vbool" >:: testBoolConstructor;
  "[Eval] Should sum positive and negative numbers" >:: testBopSum;
  "[Eval] Should invert when diff an negative number" >:: testBopDiff;
  "[Eval] Should not overflow with big numbers" >:: testBopMult;
  "[Eval] Should know that positives are always greater than negatives" >:: testBopLess;
  "[Eval] Should test for equal numbers" >:: testBopLeq;
  "[Eval] Should return false when numbers are different" >:: testBopEq;
  "[Eval] Should return false when numbers are equals" >:: testBopNeq;
  "[Eval] Should return true when equal or greater" >:: testBopGeq;
  "[Eval] Should return false when equal" >:: testBopGreater;
  "[Eval] Should resolve nested operations(big step)" >:: testNestedBops;
  "[Eval] Should raise NoRuleApplies" >:: testFailBop1;
  "[Eval] Should be true and return the first argument resolved" >:: testSimpleIfTrue;
  "[Eval] Should be false and return the second argument resolved" >:: testSimpleIfFalse;
  "[Eval] Should resolve nested if/else (big step)" >:: testNestedIfs;
  "[Eval] Should return 6! = 720" >:: testFat;
  "[Eval] Should return fib(7) = 21" >:: testFib;
  "[Eval] Should return false for odd numbers" >:: testIsEven1;
  "[Eval] Should return true for even numbers" >:: testIsEven2;
  "[Eval] Should sum two numbers" >:: testSoma;
  "[Eval] Should multiply two numbers" >:: testVezes;
  "[Eval] Should return the first element of pair" >:: testPairFst;
  "[Eval] Should return the second element of pair" >:: testPairSnd;
  "[Eval] Tests pair in middle of let expression" >:: testPairWithOpt;
  "[Eval] Check if you can 'nest' pairs" >:: testTwoPairs;
  "[Eval] Check if evaluates for pair with different types" >:: testPairDif;
  
 ]


let _ =
  run_test_tt_main suite

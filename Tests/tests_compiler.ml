open OUnit
open Compiler


(************************ EXPRESSIONS TO BE TESTED ***************************)
(* Constructors *)
let expNumConstructor = (Num (-8))
let expBoolConstructor = (Bool (true))

(* Bops *)
let expBopSum  = (Bop(Sum, Num(-3), Num(5)))                                        (* -3 + 5 *)
let expBopDiff = (Bop(Diff, Num(-3), Num(-20)))                                     (* -3 - -20 *)
let expBopMult = (Bop(Mult, Num(99999999999), Num(-31982982)))                      (* 99999999999 * -31982982 *)
let expBopLess = (Bop(Less, Num(3), Num(-5)))                                       (* 3 < -5*)
let expBopLeq = (Bop(Leq, Num(-3), Num(-3)))                                        (* -3 <= -3*)
let expBopEq = (Bop(Eq, Num(5), Num(-17)))                                          (* 5 == -17*)
let expBopNeq = (Bop(Neq, Num(5), Num(5)))                                          (* 5 != 5*)
let expBopGeq = (Bop(Geq, Num(-3), Num(-10)))                                       (* -3 => -5 *)
let expBopGreater = (Bop(Greater, Num(42), Num(42)))                                (* 42 > 42 *)
let expBopNested = (Bop(Greater,                                                    (*(3 * (100-2) - (42+42)) > (3*17 - 5) *)
                      Bop(Diff,
                        Bop(Mult,
                          Num(3),
                          Bop(Diff,
                            Num(100),
                            Num(2)
                          )
                        ),
                        Bop(Sum, Num(42), Num(42))
                      ),
                      Bop(Diff,
                        Bop(Mult, Num(3), Num(17)),
                        Num(5))))

(* Ifs *)
let expSimpleIfTrue = (If((Bool true),                                     (*if true *)
                        Num(0),                                            (*then 0 *)
                        Bop(Diff, Num(-3), Num(20))))                      (*else -3 -20*)

let expSimpleIfFalse = (If((Bool false),                                   (*if false *)
                            Num(0),                                        (*then 0 *)
                            Bop(Diff, Num(-3), Num(20))))                  (*else -3 -20*)
                        
let expNestedIf = (If( Bop(Geq, Num(8), Num(10)),                         (*If 8 > 10*)
                      If(Bop(Geq, Num(8), Num(7)),                        (*(then)should ignore this*)
                          Num(5),
                          Num(6)),
                      If(Bop(Eq, Num(3), Num(3)),                         (*else if 3 == 3*)
                            If(Bop(Neq, Num(5), Num(5)),                  (*  then if 5 != 5*)
                              Num(0),
                              Num(1)                                      (*should stop here, just think the logic throught*)
                            ),
                            Num(-2))))

(* Composed functions *)

let expFat = (Lrec("fat", TyInt, TyInt, "x", TyInt,
                If(Bop(Eq, Var("x"), Num(0)),
                  Num(1),
                  Bop(Mult, Var("x"), App(Var("fat"), Bop(Diff, Var("x"), Num(1))))),
                App(Var("fat"), Num(6))))


let expFib = (Lrec("fib", TyInt, TyInt, "x", TyInt,
                If(Bop(Leq, Var("x"),Num(1)),
                    Num(1),
                    Bop(Sum, App(Var("fib"), Bop(Diff, Var("x"), Num(1))) , App(Var("fib"), Bop(Diff, Var("x"), Num(2))))),
                App(Var("fib"), Num(7))))

let expIsEven1 = (Lrec("iseven", TyInt, TyBool, "a", TyInt,
                If(Bop(Eq, Var("a"), Num(1)),
                  Bool(false),
                      If(Bop(Eq, Var("a"), Num(0)),
                      Bool(true),
                      App(Var("iseven"),Bop(Diff, Var("a"), Num(2))))),
                App(Var("iseven"), Num(3))))

let expIsEven2 = (Lrec("iseven", TyInt, TyBool, "a", TyInt,
                  If(Bop(Eq, Var("a"), Num(1)),
                    Bool(false),
                        If(Bop(Eq, Var("a"), Num(0)),
                        Bool(true),
                        App(Var("iseven"),Bop(Diff, Var("a"), Num(2))))),
                  App(Var("iseven"), Num(300))))


let expSoma = (Lrec("soma", TyInt, TyFn(TyInt, TyInt), "a", TyInt,
                Lam("b", TyInt,
                  If(Bop(Eq, Var("b"), Num(0)),
                    Var("a"),
                    App(App(Var("soma"), Bop(Sum, Var("a"), Num(1))), Bop(Diff, Var("b"), Num(1))))
                ),
                App(App(Var("soma"), Num(-3)), Num(17))))
      
  
let expVezes = (Lrec("vezes", TyInt, TyFn(TyInt, TyInt), "a", TyInt,
                  Lam("b", TyInt,
                    If( Bop(Eq, Var("a"), Num(0)),
                      Num(0),
                      If( Bop(Eq, Var("b"), Num(0)),
                        Num(0),
                        Bop(Sum, Var("b"), App(App(Var("vezes"), Bop(Diff, Var("a"), Num(1))), Var("b")))
                      )
                    )
                ),
                App(App(Var("vezes"), Num(3)), Num(6))))

let pairSimpleFst = (Fst (Pair( Bop(Sum, Num 1, Num 2), Num 2)))
let pairSimpleSnd = (Snd (Pair( Bop(Sum, Num 1, Num 2), Num 2))) (*Embora não leve a erro de execução, não é bem tipado !*)

let pairWithOpt = (Let("pair", TyPair(TyInt, TyInt ), (Pair(Num 2, Num 4)), Bop(Sum, Fst(Var("pair")), Snd(Var("pair")))))

let twoPairs = (Let("pair1", TyPair(TyInt, TyInt), (Pair(Num 2, Num 4)),
                 (Let("pair2", TyPair(TyInt, TyInt), (Pair(Num 3, Num 4)),  
                    Bop(Sum, Bop(Mult, Fst(Var("pair1")), Fst(Var("pair2"))), Bop(Mult, Snd(Var("pair2")), Snd(Var("pair2")))
                    )
                  )
                 )
                )
              ) 

let pairDif = (Fst(Pair(Num 2, Bool true)))
              

(************************        COMPILE TESTS       *************************)
(************************ TEST FOR BASIC CONSTRUCTORS ************************)

let testNumConstructor test_ctxt = assert_equal (Vnum (-8)) (run (compile expNumConstructor)) (*constructs integer -8*)
let testBoolConstructor test_ctxt = assert_equal (Vbool true) (run (compile expBoolConstructor)) (*constructs bool true*)

(************************ TEST FOR BINARY OPERATIONS ************************)

let testBopSum' = run ( compile expBopSum )                                                      
let testBopSum test_ctxt = assert_equal (Vnum (2)) testBopSum'                        (* = 2 *)

let testBopDiff' = run ( compile expBopDiff )                                                   
let testBopDiff test_ctxt = assert_equal (Vnum (17)) testBopDiff'                     (* = 17 *)

let testBopMult' = run ( compile expBopMult )                                                   
let testBopMult test_ctxt = assert_equal (Vnum (-3198298199968017018)) testBopMult'   (* =-3198298199968017018 *)

let testBopLess' = run ( compile expBopLess )                                                   
let testBopLess test_ctxt = assert_equal (Vbool false) testBopLess'                   (* false *)

let testBopLeq' = run ( compile expBopLeq )                                                  
let testBopLeq test_ctxt = assert_equal  (Vbool true) testBopLeq'                     (* true *)

let testBopEq' = run ( compile expBopEq )                                                     
let testBopEq test_ctxt = assert_equal (Vbool false) testBopEq'                       (* false *)

let testBopNeq' = run ( compile expBopNeq )                                                      
let testBopNeq test_ctxt = assert_equal (Vbool false) testBopNeq'                     (* false *)

let testBopGeq' = run ( compile expBopGeq )                                                    
let testBopGeq test_ctxt = assert_equal (Vbool true) testBopGeq'                      (* true *)

let testBopGreater' = run ( compile expBopGreater )                                             
let testBopGreater test_ctxt = assert_equal  (Vbool false) testBopGreater'            (* false *)

let testNestedBops' = run ( compile expBopNested )
let testNestedBops test_ctxt = assert_equal (Vbool true) testNestedBops'


(************************* TEST FOR IF/ELSEs *************************)
let testSimpleIfTrue'  = run ( compile expSimpleIfTrue )
let testSimpleIfTrue'' = run ( compile (Num (0)) )
let testSimpleIfTrue test_ctxt = assert_equal testSimpleIfTrue'' testSimpleIfTrue'

let testSimpleIfFalse'  = run ( compile expSimpleIfFalse )
let testSimpleIfFalse'' = Vnum(-23)
let testSimpleIfFalse test_ctxt = assert_equal testSimpleIfFalse'' testSimpleIfFalse'


let testNestedIfs'  = run ( compile expNestedIf )
let testNestedIfs test_ctxt = assert_equal (Vnum 1) testNestedIfs'



(* EXERCISE TEST FUNCTIONS *)

let testFat' = run ( compile expFat )
let testFat test_ctxt = assert_equal (Vnum 720) testFat'

let testFib' = run ( compile expFib )
let testFib test_ctxt = assert_equal (Vnum 21) testFib'

let testIsEven1' = run ( compile expIsEven1 )
let testIsEven1 test_ctxt = assert_equal (Vbool false) testIsEven1'

let testIsEven2' = run ( compile expIsEven2 )
let testIsEven2 test_ctxt = assert_equal (Vbool true) testIsEven2'

let testSoma' = run ( compile expSoma )
let testSoma test_ctxt = assert_equal (Vnum 14) testSoma'

let testVezes' = run ( compile expVezes )
let testVezes test_ctxt = assert_equal (Vnum 18) testVezes'

let testPairFst' = run ( compile pairSimpleFst )
let testPairFst test_ctxt = assert_equal (Vnum 3) testPairFst'

let testPairSnd' = run ( compile pairSimpleSnd )
let testPairSnd test_ctxt = assert_equal (Vnum 2) testPairSnd'

let testPairWithOpt' = run ( compile pairWithOpt )
let testPairWithOpt test_ctxt = assert_equal (Vnum 6) testPairWithOpt'

let testTwoPairs' = run ( compile twoPairs )
let testTwoPairs test_ctxt = assert_equal (Vnum 22) testTwoPairs'

let testPairDif' = run ( compile pairDif )
let testPairDif test_ctxt = assert_equal (Vnum 2) testPairDif'

(************************      COMPILE TESTS       ***************************)
(************************ TEST FOR BASIC CONSTRUCTORS ************************)


let suite =
"suite" >:::
 [
  "[Compile] Should construct a negative Vnum" >:: testNumConstructor;
  "[Compile] Should construct a true Vbool" >:: testBoolConstructor;
  "[Compile] Should sum positive and negative numbers" >:: testBopSum;
  "[Compile] Should invert when diff an negative number" >:: testBopDiff;
  "[Compile] Should not overflow with big numbers" >:: testBopMult;
  "[Compile] Should know that positives are always greater than negatives" >:: testBopLess;
  "[Compile] Should test for equal numbers" >:: testBopLeq;
  "[Compile] Should return false when numbers are different" >:: testBopEq;
  "[Compile] Should return false when numbers are equals" >:: testBopNeq;
  "[Compile] Should return true when equal or greater" >:: testBopGeq;
  "[Compile] Should return false when equal" >:: testBopGreater;
  "[Compile] Should resolve nested operations" >:: testNestedBops;
  "[Compile] Should be true and return the first argument resolved" >:: testSimpleIfTrue;
  "[Compile] Should be false and return the second argument resolved" >:: testSimpleIfFalse;
  "[Compile] Should resolve nested if/else" >:: testNestedIfs;
  "[Compile] Should return 6! = 720" >:: testFat;
  "[Compile] Should return fib(7) = 21" >:: testFib;
  "[Compile] Should return false for odd numbers" >:: testIsEven1;
  "[Compile] Should return true for even numbers" >:: testIsEven2;
  "[Compile] Should sum two numbers" >:: testSoma;
  "[Compile] Should multiply two numbers" >:: testVezes;
  "[Compile] Should return the first element of pair" >:: testPairFst;
  "[Compile] Should return the second element of pair" >:: testPairSnd;
  "[Compile] Tests pair in middle of let expression" >:: testPairWithOpt;
  "[Compile] Check if you can 'nest' pairs" >:: testTwoPairs;
  "[Compile] Check if evaluates for pair with different types" >:: testPairDif; 
  ]


let _ =
  run_test_tt_main suite
